#ifndef _GPIO_KEY_H_
#define _GPIO_KEY_H_

#define USER_KEY_PIN     27

#define USER_KEY_PIN_SEL  (1ULL<<USER_KEY_PIN)

void gpio_key_init(void);

#endif

