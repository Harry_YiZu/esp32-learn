/* MQTT (over TCP) Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"
#include "os.h"
#include "cJSON.h"
#include "user_mqtt.h"
#include "driver/gpio.h"
#include "gpio_key.h"



static const char *TAG = "MQTT_EXAMPLE";

/*！！！！！！如下信息请替换成自己在阿里云的信息！！！！！！*/
//方法1
/*由阿里网平台可得如下信息*/
// ProductKey:"a1tUbQR2faQ"; 
// DeviceName:"dev-esp32"; 
// DeviceSecret:"e624520f169c755e0d02cd0ca99c94c1";
// Region:"cn-shanghai";     

/*下面一参数为自定义*/
// ClientID:="112233";       

// /*Broker Address：${YourProductKey}.iot-as-mqtt.${YourRegionId}.aliyuncs.com*/
// #define   Aliyun_host       "a1tUbQR2faQ.iot-as-mqtt.cn-shanghai.aliyuncs.com"   /*或称mqttHostUrl、Broker Address*/
// #define   Aliyun_port       1883
// /*Client ID：     ${ClientID}|securemode=${Mode},signmethod=${SignMethod}|*/
// #define   Aliyun_client_id  "112233|securemode=2,signmethod=hmacsha1|"
// /*User Name：     ${DeviceName}&${ProductKey}*/
// #define   Aliyun_username   "dev-esp32&a1tUbQR2faQ"
// /*使用官网 MQTT_Password 工具生成*/
// #define   Aliyun_password   "9ABE732ED28BC38EEE7336FA824C26E744413360"

// #define   AliyunSubscribeTopic_user_get     "/a1tUbQR2faQ/dev-esp32/user/get"
// #define   AliyunPublishTopic_user_update    "/a1tUbQR2faQ/dev-esp32/user/update"

//方法2
#define   Aliyun_host       "a1nZ5UaE6cl.iot-as-mqtt.cn-shanghai.aliyuncs.com"   /*或称mqttHostUrl、Broker Address*/
#define   Aliyun_port       1883
#define   Aliyun_client_id  "a1nZ5UaE6cl.dev-esp32-com5|securemode=2,signmethod=hmacsha256,timestamp=1669105552434|"
#define   Aliyun_username   "dev-esp32-com5&a1nZ5UaE6cl"
#define   Aliyun_password   "5e14b9f54a10314733020962db96f2f7fe5e06f7784457fa2a1140e2f340c371"

#define   AliyunSubscribeTopic_user_get     "/a1nZ5UaE6cl/dev-esp32-com5/user/get"
#define   AliyunPublishTopic_user_update    "/a1nZ5UaE6cl/dev-esp32-com5/user/update"

char mqtt_publish_data1[] = "mqtt connect ok ";
char mqtt_publish_data2[] = "mqtt subscribe successful";
char mqtt_publish_data3[] = "mqtt i am esp32";
#define   LOCAL_DATA_SIZE     128
char local_data_tx[LOCAL_DATA_SIZE] = {0}; 
char local_data_rx[LOCAL_DATA_SIZE] = {0}; 

esp_mqtt_client_handle_t client;

static int user_parse_json(char *json_data)
{
	cJSON *root = NULL;

    root = cJSON_Parse(json_data);   
    if (!root) 
    {
      printf("Error before: [%s]\n",cJSON_GetErrorPtr());
	  return  -1;
	}
    printf("%s\n\n", cJSON_Print(root));   /*将完整的数据以JSON格式打印出来*/

    return 0;
}

static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    int  msg_id;
	
    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            // msg_id = esp_mqtt_client_publish(client, AliyunPublishTopic_user_update, mqtt_publish_data1, strlen(mqtt_publish_data1), 1, 0);
            // ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);

            msg_id = esp_mqtt_client_subscribe(client, AliyunSubscribeTopic_user_get, 0);
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            // msg_id = esp_mqtt_client_publish(client, AliyunPublishTopic_user_update, mqtt_publish_data2, strlen(mqtt_publish_data2), 0, 0);
            // ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
            printf("DATA=%.*s\r\n", event->data_len, event->data);
            memset(local_data_rx,0x0,LOCAL_DATA_SIZE);		
            strncpy(local_data_rx, event->data, event->data_len); //将指针类型的数据复制到一个数组中
            msg_id = user_parse_json(local_data_rx);
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqtt_event_handler_cb(event_data);
}

static void mqtt_test_task(void *pvParameters)
{
    int  msg_id;
    char key_flag = 0;
    //构造JSON格式数据，该数据用于反馈给阿里云物联网平台，
    cJSON *Wroot =	cJSON_CreateObject();

    cJSON_AddItemToObject(Wroot, "DeviceSource", cJSON_CreateString("ESP32-COM5"));
    cJSON_AddItemToObject(Wroot, "DevicePurpose", cJSON_CreateString("ESP32-COM4"));
    cJSON_AddItemToObject(Wroot, "Data", cJSON_CreateString("hello ,I am COM5"));
    cJSON_AddItemToObject(Wroot, "Date", cJSON_CreateString("555555"));

    //printf("%s\n", cJSON_Print(Wroot)); //打印刚才构建的JSON数据，看是否正确

    memset(local_data_tx,0x0,LOCAL_DATA_SIZE);		
    int len = strlen(cJSON_Print(Wroot));			
    memcpy(local_data_tx, cJSON_Print(Wroot),len); //将JSON格式数据复制到数组中，将以数组的形式传递给发布函数
    // local_data_tx[len] = '\0';
    // printf("%s\n",local_data_tx);//打印数据是否正确
    //发布信息到阿里云物联网平台
    // msg_id = esp_mqtt_client_publish(client, AliyunPublishTopic_user_update, local_data_buffer, strlen(local_data_buffer), 0, 0);
    // ESP_LOGI(TAG, "esp32 com4 send data successful, msg_id=%d", msg_id);		
    // cJSON_Delete(Wroot);  //执行这个函数会出错，不知道什么原因
    // cJSON_Delete(Pitem);
    for(;;)
    {
        if(0 == gpio_get_level(USER_KEY_PIN)){
            vTaskDelay(10 / portTICK_RATE_MS);
            while(0 == gpio_get_level(USER_KEY_PIN)){
                if(key_flag == 0){
                    key_flag = 1;
                    printf("key press  \n");
                    msg_id = esp_mqtt_client_publish(client, AliyunPublishTopic_user_update, local_data_tx, strlen(local_data_tx), 0, 0);
                    ESP_LOGI(TAG, "esp32 com5 send data successful, msg_id=%d", msg_id);	
                }
            }
	    }else{
            key_flag = 0;
        }
	}
    vTaskDelete(NULL);
}


void user_mqtt_app_start(void)
{
    esp_mqtt_client_config_t mqtt_cfg = {
		.host = Aliyun_host,
		.port = Aliyun_port,
		.client_id = Aliyun_client_id,
		.username = Aliyun_username,
		.password = Aliyun_password,

    };

    client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
    esp_mqtt_client_start(client);
	
	xTaskCreate(&mqtt_test_task, "mqtt_test_task", 4096, NULL, 5, NULL);
}

