/* MQTT (over TCP) Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"
#include "os.h"

#include "user_mqtt.h"



static const char *TAG = "MQTT_EXAMPLE";

/*！！！！！！如下信息请替换成自己在阿里云的信息！！！！！！*/
//方法1
/*由阿里网平台可得如下信息*/
// ProductKey:"a1tUbQR2faQ"; 
// DeviceName:"dev-esp32"; 
// DeviceSecret:"e624520f169c755e0d02cd0ca99c94c1";
// Region:"cn-shanghai";     

/*下面一参数为自定义*/
// ClientID:="112233";       

// /*Broker Address：${YourProductKey}.iot-as-mqtt.${YourRegionId}.aliyuncs.com*/
// #define   Aliyun_host       "a1tUbQR2faQ.iot-as-mqtt.cn-shanghai.aliyuncs.com"   /*或称mqttHostUrl、Broker Address*/
// #define   Aliyun_port       1883
// /*Client ID：     ${ClientID}|securemode=${Mode},signmethod=${SignMethod}|*/
// #define   Aliyun_client_id  "112233|securemode=2,signmethod=hmacsha1|"
// /*User Name：     ${DeviceName}&${ProductKey}*/
// #define   Aliyun_username   "dev-esp32&a1tUbQR2faQ"
// /*使用官网 MQTT_Password 工具生成*/
// #define   Aliyun_password   "9ABE732ED28BC38EEE7336FA824C26E744413360"

// #define   AliyunSubscribeTopic_user_get     "/a1tUbQR2faQ/dev-esp32/user/get"
// #define   AliyunPublishTopic_user_update    "/a1tUbQR2faQ/dev-esp32/user/update"

//方法2
#define   Aliyun_host       "a1Buwr73pUI.iot-as-mqtt.cn-shanghai.aliyuncs.com"   /*或称mqttHostUrl、Broker Address*/
#define   Aliyun_port       1883
#define   Aliyun_client_id  "a1Buwr73pUI.dev-esp32-com4|securemode=2,signmethod=hmacsha256,timestamp=1668848249078|"
#define   Aliyun_username   "dev-esp32-com4&a1Buwr73pUI"
#define   Aliyun_password   "9b06793c079498fb5e7b8f10f9de8fd9de7af8601b59b82a579229079dcef234"

#define   AliyunSubscribeTopic_user_get     "/a1Buwr73pUI/dev-esp32-com4/user/get"
#define   AliyunPublishTopic_user_update    "/a1Buwr73pUI/dev-esp32-com4/user/update"

char mqtt_publish_data1[] = "mqtt connect ok ";
char mqtt_publish_data2[] = "mqtt subscribe successful";
char mqtt_publish_data3[] = "mqtt i am esp32";

esp_mqtt_client_handle_t client;




static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    int  msg_id;
	
    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            msg_id = esp_mqtt_client_publish(client, AliyunPublishTopic_user_update, mqtt_publish_data1, strlen(mqtt_publish_data1), 1, 0);
            ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);

            msg_id = esp_mqtt_client_subscribe(client, AliyunSubscribeTopic_user_get, 0);
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            msg_id = esp_mqtt_client_publish(client, AliyunPublishTopic_user_update, mqtt_publish_data2, strlen(mqtt_publish_data2), 0, 0);
            ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
            printf("DATA=%.*s\r\n", event->data_len, event->data);
			
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqtt_event_handler_cb(event_data);
}

static void mqtt_test_task(void *pvParameters)
{
    uint8_t num = 0;

    while(1)
    {
       esp_mqtt_client_publish(client, AliyunPublishTopic_user_update, mqtt_publish_data3, strlen(mqtt_publish_data3), 1, 0);
       vTaskDelay(2000 / portTICK_PERIOD_MS);
	   if(num++ > 5) break;
	}
    vTaskDelete(NULL);
}


void user_mqtt_app_start(void)
{
    esp_mqtt_client_config_t mqtt_cfg = {
		.host = Aliyun_host,
		.port = Aliyun_port,
		.client_id = Aliyun_client_id,
		.username = Aliyun_username,
		.password = Aliyun_password,

    };

    client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
    esp_mqtt_client_start(client);
	
	xTaskCreate(&mqtt_test_task, "mqtt_test_task", 4096, NULL, 5, NULL);
}

