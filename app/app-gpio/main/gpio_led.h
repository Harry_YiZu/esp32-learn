#ifndef _GPIO_LED_H_
#define _GPIO_LED_H_

#define USER_LED_PIN     26

#define USER_LED_PIN_SEL  (1ULL<<USER_LED_PIN)

void gpio_led_init(void);

#endif

