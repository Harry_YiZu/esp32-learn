#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "gpio_key.h"


static void gpio_task_key(void* arg);


void gpio_key_init(void)
{
    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_INPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = USER_KEY_PIN_SEL;
    //disable pull-up mode
    io_conf.pull_up_en = 1;
    //configure GPIO with the given settings
    gpio_config(&io_conf);

	//start gpio task
    xTaskCreate(gpio_task_key, "gpio_task_key", 2048, NULL, 9, NULL);

}


static void gpio_task_key(void* arg)
{
    for(;;) 
	{
	   
       if(0 == gpio_get_level(USER_KEY_PIN))
       {
          vTaskDelay(10 / portTICK_RATE_MS);
		  if(0 == gpio_get_level(USER_KEY_PIN))
	  	  {
	  	    printf("key press  \n");
	  	  }
	   }
	   vTaskDelay(10 / portTICK_RATE_MS);
    }
}






