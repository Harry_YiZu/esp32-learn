#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "gpio_led.h"

static void gpio_task_led(void* arg);


void gpio_led_init(void)
{
    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = USER_LED_PIN_SEL;
    //disable pull-down mode，作为输出不需要下拉模式
    io_conf.pull_down_en = 0;
    //disable pull-up mode，作为输出不需要上拉模式
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    gpio_config(&io_conf);

    //start gpio task
    xTaskCreate(gpio_task_led, "gpio_task_led", 2048, NULL, 11, NULL);

}

static void gpio_task_led(void* arg)
{
    int led_cnt = 0;
    for(;;) 
	{	 
	   led_cnt++;
	   vTaskDelay(500 / portTICK_RATE_MS);
	   gpio_set_level(USER_LED_PIN, led_cnt % 2);
	   printf("led status:%d  \n",(led_cnt % 2));
    }
}





