#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 闃块噷浜戠墿鑱旂綉骞冲彴OTA杩斿洖鏁版嵁锛?鏁版嵁缁撴瀯浣?
 */
typedef struct {
	char *code ;
	int size ;
	char *_package_udi;
	char *sign;
	char *version;
	char *url;
	char *signMethod;
	char *md5;
	double id;
	char *message;

} user_aliyun_ota_config_t;


esp_err_t user_parse_json(char *json_data,user_aliyun_ota_config_t ota_config);




#ifdef __cplusplus
}
#endif

#include "aliyun_ota.h"



static const char *TAG = "MQTT_EXAMPLE";

/*！！！！！！如下信息请替换成自己在阿里云的信息！！！！！！*/

/*由阿里网平台可得如下信息*/
// ProductKey:"a1tUbQR2faQ"; 
// DeviceName:"dev-esp32"; 
// DeviceSecret:"e624520f169c755e0d02cd0ca99c94c1";
// Region:"cn-shanghai";     

/*下面一参数为自定义*/
// ClientID:="112233";       



/*Broker Address：${YourProductKey}.iot-as-mqtt.${YourRegionId}.aliyuncs.com*/
#define   Aliyun_host       "a1tUbQR2faQ.iot-as-mqtt.cn-shanghai.aliyuncs.com"
#define   Aliyun_port       1883
/*Client ID：     ${ClientID}|securemode=${Mode},signmethod=${SignMethod}|*/
#define   Aliyun_client_id  "112233|securemode=2,signmethod=hmacsha1|"
/*User Name：     ${DeviceName}&${ProductKey}*/
#define   Aliyun_username   "dev-esp32&a1tUbQR2faQ"
/*使用官网 MQTT_Password 工具生成*/
#define   Aliyun_password   "9ABE732ED28BC38EEE7336FA824C26E744413360"


#define   AliyunSubscribeTopic_user_get     "/a1tUbQR2faQ/dev-esp32/user/get"
#define   AliyunPublishTopic_user_update    "/a1tUbQR2faQ/dev-esp32/user/update"
#define   AliyunSubscribeTopic_ota_upgrade  "/ota/device/upgrade/a1tUbQR2faQ/dev-esp32"
#define   AliyunPublishTopic_ota_inform     "/ota/device/inform/a1tUbQR2faQ/dev-esp32"

char local_data_buffer[1024] = {0};  


char mqtt_publish_data1[] = "mqtt connect ok ";
char mqtt_publish_data2[] = "mqtt subscribe successful";
char mqtt_publish_data3[] = "mqtt i am esp32";

esp_mqtt_client_handle_t client;
user_aliyun_ota_config_t aliyun_ota_config;



static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
{
    esp_err_t ret;

    esp_mqtt_client_handle_t client = event->client;
    int  msg_id;
	uint8_t temp;
	
	
    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            msg_id = esp_mqtt_client_publish(client, AliyunPublishTopic_user_update, mqtt_publish_data1, strlen(mqtt_publish_data1), 1, 0);
            ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);

            msg_id = esp_mqtt_client_subscribe(client, AliyunSubscribeTopic_user_get, 0);
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            msg_id = esp_mqtt_client_publish(client, AliyunPublishTopic_user_update, mqtt_publish_data2, strlen(mqtt_publish_data2), 0, 0);
            ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
           // printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
           // printf("DATA=%.*s\r\n", event->data_len, event->data);
           
            char *topicBuf = (char*)malloc(event->topic_len+1);
		   //void  *memcpy(void *s1,  const void *s2,  size_t  n); 函数memcpy从s2指向的对象中复制n个字符到s1指向的对象中。		   
           	memcpy(topicBuf, event->topic, event->topic_len);

			//printf("---Receive topic:\n %s \r\n", topicBuf);//打印接收的消息
			temp = strcmp(topicBuf,AliyunSubscribeTopic_ota_upgrade);//判断主题是否是OTA主题，如果是才调用OTA的JSON解析函数

		    if(0 == temp)//是OTA升级主题	
		    {	
		    	ESP_LOGI(TAG, "OTA DATA PARSE");
		        //char  *strncpy(char *s2, const char *s1, size_t n);函数strncpy从s1指向的数组中最多复制n个字符（不复制空字符后面的字符）到s2指向的数组中。
				strncpy(local_data_buffer, event->data, event->data_len); //将指针类型的数据复制到一个数组中
				//printf("local_data_buffer:%s ", local_data_buffer); 
			    ret = user_parse_json(local_data_buffer,aliyun_ota_config);//解析数据，并使用解析出来的URL下载OTA升级包
				if (ret == ESP_OK) //解析并下载OTA升级包成功
				{
			        //构造JSON格式数据，该数据用于反馈给阿里云物联网平台，作用是通知升级包接收完成
					cJSON *Wroot =	cJSON_CreateObject();
					cJSON *Pitem =	cJSON_CreateObject();
					
					cJSON_AddItemToObject(Wroot, "id", cJSON_CreateString("123"));
					cJSON_AddItemToObject(Wroot, "params", Pitem);
					cJSON_AddItemToObject(Pitem, "step", cJSON_CreateString("100"));
					cJSON_AddItemToObject(Pitem, "desc", cJSON_CreateString("OTA update successfully !"));
					cJSON_AddItemToObject(Pitem, "module", cJSON_CreateString("MCU"));

				
					//printf("%s\n", cJSON_Print(Wroot)); //打印刚才构建的JSON数据，看是否正确

					char ota_inform_buf[512];
					int len = strlen(cJSON_Print(Wroot));					
					memcpy(ota_inform_buf, cJSON_Print(Wroot),len); //将JSON格式数据复制到数组中，将以数组的形式传递给发布函数
					ota_inform_buf[len] = '\0';
					printf("%s\n",ota_inform_buf);//打印数据是否正确
					//发布信息到阿里云物联网平台
		            msg_id = esp_mqtt_client_publish(client, AliyunPublishTopic_ota_inform, ota_inform_buf, strlen(ota_inform_buf), 0, 0);
		            ESP_LOGI(TAG, "sent publish ota inform successful, msg_id=%d", msg_id);		
					
//					cJSON_Delete(Wroot);  //执行这个函数会出错，不知道什么原因
//					cJSON_Delete(Pitem);
					esp_restart();  //ESP32设备重启，重启后将执行刚才下载的程序
				} 
				else 
				{
					ESP_LOGE(TAG, "Firmware upgrade failed");
			    }	


		    }
		    free(topicBuf);			
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}




static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqtt_event_handler_cb(event_data);
}

static void mqtt_test_task(void *pvParameters)
{
    uint8_t num = 0;

    ESP_LOGI(TAG, "into mqtt_test_task ");  
    while(1)
    {
       vTaskDelay(1000 / portTICK_PERIOD_MS);
	   if(num++ > 5) 
	   {
		  num = 0;
		  ESP_LOGI(TAG, "while---");   		   
	   }
	}
    vTaskDelete(NULL);
}


void user_mqtt_app_start(void)
{
    esp_mqtt_client_config_t mqtt_cfg = {
		.host = Aliyun_host,
		.port = Aliyun_port,
		.client_id = Aliyun_client_id,
		.username = Aliyun_username,
		.password = Aliyun_password,

    };

    client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
    esp_mqtt_client_start(client);
	
	xTaskCreate(&mqtt_test_task, "mqtt_test_task", 4096, NULL, 5, NULL);
}

