/* OTA example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"

#include "string.h"
#include "mqtt_client.h"


#include "cJSON.h"

#include "aliyun_ota.h"

#define OTA_URL_SIZE 256 


static const char *TAG = "simple_ota_example";
extern const uint8_t server_root_crt_start[] asm("_binary_root_crt_start");
extern const uint8_t server_root_crt_end[]   asm("_binary_root_crt_end");



 esp_err_t _http_event_handler(esp_http_client_event_t *evt)
 {
     switch (evt->event_id) {
     case HTTP_EVENT_ERROR:
  	   ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
  	   break;
     case HTTP_EVENT_ON_CONNECTED:
  	   ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
  	   break;
     case HTTP_EVENT_HEADER_SENT:
  	   ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
  	   break;
     case HTTP_EVENT_ON_HEADER:
  	   ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
  	   break;
     case HTTP_EVENT_ON_DATA:
  	   ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
  	   break;
     case HTTP_EVENT_ON_FINISH:
  	   ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
  	   break;
     case HTTP_EVENT_DISCONNECTED:
  	   ESP_LOGD(TAG, "HTTP_EVENT_DISCONNECTED");
  	   break;
     }
     return ESP_OK;
 }


 esp_err_t user_parse_json(char *json_data,user_aliyun_ota_config_t ota_config)
{
	
	
	cJSON *item = NULL;
	cJSON *root = NULL;

//root 开始-------------------------------------------------------------------------		
    root = cJSON_Parse(json_data);   /*json_data 阿里云OTA原始数据*/ 
    if (!root) 
    {
      printf("Error before: [%s]\n",cJSON_GetErrorPtr());	  
	  return  ESP_FAIL;
	}
	printf("%s\n\n", cJSON_Print(root));   /*将完整的数据以JSON格式打印出来*/
	
	item = cJSON_GetObjectItem(root, "code");
	ota_config.code = cJSON_Print(item);
	
//data 开始-------------------------------------------------------------------------	    		
	cJSON *Pdata = cJSON_GetObjectItem(root, "data");	
    item = cJSON_GetObjectItem(Pdata, "size");
    ota_config.size = item->valueint;
    
//extData 开始-------------------------------------------------------------------------		
	cJSON *PextData = cJSON_GetObjectItem(Pdata, "extData");
	item = cJSON_GetObjectItem(PextData, "_package_udi");
	ota_config._package_udi = cJSON_Print(item);  
//extData 结束------------------------------------------------------------------------- 	


	item = cJSON_GetObjectItem(Pdata, "sign");
	ota_config.sign = cJSON_Print(item);    

	item = cJSON_GetObjectItem(Pdata, "version");
	ota_config.version = cJSON_Print(item);    

	item = cJSON_GetObjectItem(Pdata, "url");
	ota_config.url = cJSON_Print(item);    

	item = cJSON_GetObjectItem(Pdata, "signMethod");
	ota_config.signMethod = cJSON_Print(item);    

	item = cJSON_GetObjectItem(Pdata, "md5");
	ota_config.md5 = cJSON_Print(item);
//data 结束-------------------------------------------------------------------------

	item = cJSON_GetObjectItem(root, "id");
	ota_config.id = item->valuedouble;
    
	item = cJSON_GetObjectItem(root, "message");
	ota_config.message = cJSON_Print(item);
//root 结束-------------------------------------------------------------------------    
//   cJSON_Delete(root);   //执行这个函数会出错，不知道什么原因
//   cJSON_Delete(Pdata);
//   cJSON_Delete(PextData);
//   cJSON_Delete(item);



//  printf("code:%s\n", ota_config.code);
//  printf("size:%d\n", ota_config.size);	
//  printf("_package_udi:%s\n", ota_config._package_udi);
//  printf("sign:%s\n", ota_config.sign);	
//	printf("version:%s\n", ota_config.version);
//  printf("url:%s\n", ota_config.url);	
//  printf("signMethod:%s\n", ota_config.signMethod);	
//  printf("md5:%s\n", ota_config.md5);	
//	printf("id:%f\n", ota_config.id);	
//	printf("message:%s\n", ota_config.message);	


	  char url_buf[OTA_URL_SIZE];

	  int len = strlen(ota_config.url);

	  memcpy(url_buf, ota_config.url, len); //将指针型数据复制到数组中，

//解析出来的URL包含了双引号""，但数组中的URL不能包含双引号""
	  for(int i = 0;i < len;i++ )
	  {
	     url_buf[i] = url_buf[i+1];   //为了去掉数组中的双引号“”，这里是前部分
	  }

	  url_buf[len - 2] = '\0';       //为了去掉数组中的双引号“”，这里是后部分

	  printf("buf:%s\n", url_buf);  //打印检查URL是否正确


	  esp_http_client_config_t config = {	  
	  	//. url = "https://xxx"  //这里需要双引号""
	      .url = url_buf,        //这里，数组中不能包含双引号""
		  .cert_pem = (char *)server_root_crt_start, 
		  .event_handler = _http_event_handler,
	  };
		
    esp_err_t ret = esp_https_ota(&config); //下载升级包等一系列操作

	return  ret;
}





