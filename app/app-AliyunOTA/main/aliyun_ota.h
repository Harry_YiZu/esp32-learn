#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 闃块噷浜戠墿鑱旂綉骞冲彴OTA杩斿洖鏁版嵁锛?鏁版嵁缁撴瀯浣?
 */
typedef struct {
	char *code ;
	int size ;
	char *_package_udi;
	char *sign;
	char *version;
	char *url;
	char *signMethod;
	char *md5;
	double id;
	char *message;

} user_aliyun_ota_config_t;


esp_err_t user_parse_json(char *json_data,user_aliyun_ota_config_t ota_config);




#ifdef __cplusplus
}
#endif

